package com.yao2san;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
public class SimTaskServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(SimTaskServerApplication.class, args);
    }
}
